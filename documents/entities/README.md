# Entities
This folder contains the schemas for all of loom's entities.

### What is an entity?
This project uses the term _entity_ to refer to any object that has a dedicated
table in the server's database.

For example:
| Entity  | Table    |
|:------- |:-------- |
| User    | users    |
| Chat    | chats    |
| Message | messages |

## Relational Entities
These are entities that a related to other entities. For example a message and
its author (a user).

### Nesting vulnerability
This behaviour creates a risk for very nested queries, for example:
```
query AttackQuery($chat_id: ID!, $message_id: ID!) {
	chat(id: $chat_id) {
		message(id: $message_id) {
			chat {
				message(id: $message_id) {
					...
				}
			}
		}
	}
}
```
Making the query more complicated can increase the stress on the backend
exponentially and therefore it is extremely recommended to include protection
with any loom implementation.

### Possible methods of protection
These are some common methods of protecting from such attacks. However, they are
not the only ones.

#### Query complexity limiting
This method involves calculating a complexity for all queries, and rejecting
queries with a complexity that is too large. For example with the
[gqlgen](https://gqlgen.com) library for [Go](https://golang.org) you can start
by following [this page](https://gqlgen.com/reference/complexity).

#### Query timeouts
Although query timeouts should be implemented anyway, they can be used to avoid
these queries from running too long.
