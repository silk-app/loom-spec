# Loom API specifications
This repository contains the Loom API specifications. Any Loom server must
implement all of the specifications in this repository to be considered as fully
functional.

## Aim
The aim of this project is to provide a set of API specifications for and open,
free and distributed community communication platform. It aims to allow easy
hosting of a personal Loom API server with federation with other servers to
allow for efficient and predictable decentralisation.

## Security
Since this project will try to encourage users to host their own Loom API
servers, for the initial designs data stored on the server will be considered
"secure", that is it will be assumed that all data stored is intentionally not
hidden from the Loom API server owner.

### Why?
The final goal of the project is to have a platform where each community has its
own Loom API server, and therefore should not have the need for end-to-end
encryption since the community administrators will want to have access to raw
data for moderation, therefore resulting in the stated assumption.

Since this is a project aiming to provide a platform for community
communications rather than personal communications, although direct messaging
is still a part of the project, this assumption will remain until at least the
first stable version.

### E2E Encryption
As a consequence of this initial assumption, there are no initial plans to
implement end-to-end encryption since the server owner is intended to have
access to the raw data anyway.

However, the project will implement end-to-end encryption at a point where
either:
- the initial goal of the project is considered mostly accomplished.
- there is a lot of demand from the active user-base.

### Federated user safety
Due to the decentralised goals of the project, there will be future research and
sub-projects on implementing a federated user safety system.

This system will try to allow users / entire Loom API servers to be
banned / ignored in an entire federation without all the other Loom API servers
having to explicitly ban / ignore them.

This system will become the highest priority once the project goes into beta,
since in the majority of social platforms user safety is the biggest security
concern for all users of the platform, which is especially true for this project
since it already maximises the other concern - data security.

## Versioning
### Major
The Loom API specifications will only increase major versions for breaking
changes. To minimise the number of actively supported major versions, the
project will try to combine as many as possible planned breaking changes into
each major version.

More detailed information to how major versions will be
managed will come later when the project matures to at least alpha levels of
stability.

### Minor
The Loom API specifications will not aim to keep track of minor versions since
they will never contain breaking changes. Therefore, to still be able to refer
to an exact version, the corresponding git commit should be tracked.

## FAQ
### Why a new project when matrix.org exists?
The Matrix protocol is definitely a very mature protocol that initially seems to
have solved what this project is trying to accomplish. However, the matrix
protocol has its largest emphasis on data security and cross-platform bridging.

This project, as stated, has a very different goal that tries to server
communities first and foremost, that is it will encourage platform features and
usability much more.

We do not doubt that this project will co-exist with matrix and there are plans
to have a functional bridge implemented into the specifications later on that
will turn a Loom API server into a fully-functional Matrix homeserver.

### What about the many other projects such as Mumble or Rocket.chat?
Many of these projects are either enterprise-oriented, rather than
public community-oriented which results in a focus on a very different set of
features; or they are primarily a VoIP platform, whereas this project will aim
to provide advanced chatting functionality before any VoIP functionality.
